# Patch release runbook

**NOTE: The guidelines described in this document only apply during the GitLab [internal pilot] on 15.10
and once the GitLab maintenance policy has been extended**

For reference the current documentation about patch releases can be found the [patch release docs].
_________

## General guidelines

As defined on the [maintenance policy], patch releases can only include
bug fixes for the current stable released version of GitLab.

Bug fixes can be backported to the current version if:

* They have been previously fixed and merged into the GitLab default branch.
* They have been deployed to GitLab.com.
* They are bug fixes. Per the maintenance policy [patch release guidelines], features can't be backported to previous versions

## Process for engineers.

Engineers should ensure the bug fixes to be backported satisfy the [general guidelines](#general-guidelines) described above.

### [GitLab project]

Once it has been deemed a bug fix should be backported, engineers should:

1. Ensure the bug fixes comply with the [general guidelines](#general-guidelines): The original MR is a bug fix that has been deployed to GitLab.com.
1. Open up a merge request in the GitLab canonical project. The merge request should target
   the stable branch associated with the current version.
1. Use the [stable branch template] and follow the check list.
1. Ensure the merge request is approved by a maintainer. Merge requests targeting stable branches
   only require one approval.
1. Add a [severity label] to the merge request (if applicable). Severity labels helps release managers assess the urgency of
   a patch release.
1. Ensure the [`package-and-test` pipeline] is executed. This pipeline executes end-to-end testing for the GitLab platform
   guaranteeing the code chages are compliant from the Quality side.
1. Verify the `package-and-test` is successfully executed. If failures arise, contact a Software in Test (SET) engineer to
   review if the failure is related to the merge request.

### Projects under [Managed Versioning]

Once it has been deemed a bug fix should be backported, engineers should:

1. Ensure the bug fixes comply with the [general guidelines](#general-guidelines): The original MR is a bug fix that has been deployed to GitLab.com.
1. Open up a merge request in the respective canonical project. The merge request should target
   the stable branch associated with the current version.
1. Use the stable branch template and follow the checklist
   * [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md)
   * [CNG](https://gitlab.com/gitlab-org/build/CNG/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md)
1. Ensure the merge request is approved by a maintainer. Merge requests targeting stable branches
   only require one approval.
1. Add a [severity label] to the merge request (if applicable). Severity labels helps release managers assess the urgency of
   a patch release.

## Process for maintainers

### [GitLab project]

When a merge request targeting a stable branch is assigned to maintainers, they should:

1. Confirm the merge request being backported has been deployed to GitLab.com
1. Verify only bug fixes are being backported. Per the [maintenance policy], features can't be backported.
1. Ensure the [`package-and-test` pipeline] has been executed.
1. Verify if there are failures on the `package-and-test` pipeline. Due to [test flakiness], this
   pipeline is allowed to fail, and as a consequence, it won't cause a failure on the upstream GitLab pipeline.
   Failures on this pipeline should be reviewed by a Software Engineer in Test (SET).
   **Don't merge the backport until a SET has confirmed the failures are not related.**

### Projects under [Managed Versioning]

When a merge request targeting a stable branch is assigned to maintainers, they should:

1. Ensure the merge request being backported has been deployed to GitLab.com
1. Verify only bug fixes are being backported. Per the [maintenance policy], features can't be backported.

## Additional considerations

1. After a backport is merged into a stable branch, release managers will assess the patch release pressure
   to determine if a patch release is required. Thats why it is important to add a [severity label] to the
   merge requests.
1. Keep an eye in Slack development and releases channels. When a patch release is published a notification
   is sent to multiple slack channels to broadcast the patch release.
1. Delivery is working on generating [long lived enviroments] to be continuously deployed from stable branches,
   as part of this effort, on the a downstream pipeline called `start-release-environments-pipeline` is automatically
   triggered on commits on stable branches on the [GitLab canonical project], this one is allowed to fail, and such,
   failures can be temporarily ignored.

## Feedback and questions

Feedback and/or questions can be added to the [internal pilot] issue. If urgent, please post a message on the
[#releases] Slack channel.

[patch release docs]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/process.md
[internal pilot]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2886
[patch release docs]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/process.md
[maintenance policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[patch release guidelines]: https://docs.gitlab.com/ee/policy/maintenance.html#patch-releases
[GitLab project]: https://gitlab.com/gitlab-org/gitlab
[`package-and-test` pipeline]: https://docs.gitlab.com/ee/development/testing_guide/end_to_end/package_and_test_pipeline.html
[Managed Versioning]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/index.md
[GitLab canonical project]: https://gitlab.com/gitlab-org/gitlab
[stable branch template]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md
[severity label]: https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity
[test flakiness]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/test-metrics-dashboards/#package-and-test
[#releases]: https://gitlab.slack.com/archives/C0XM5UU6B
[long lived environments]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/837
