# Monthly Release

GitLab releases a new minor version (`X.Y`) every month on the 22nd. The history
and reasoning behind this schedule can be found [on the blog].

For an idea of the release process please see the [monthly template](https://gitlab.com/gitlab-org/release-tools/blob/master/templates/monthly.md.erb).

Release Managers only create blog posts for patch releases that are not security
related.  The security team handles the latter.  And for major and minor
releases, a dedicated team exists for these posts.

Follow our documentation for the monthly release in our auto-deploy
documentation here: [Auto-Deploy
Process](./deploy/auto-deploy.md#process_overview)

---

[Return to Guides](../README.md#guides)

[on the blog]: https://about.gitlab.com/2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab/
[release task tracker]: https://gitlab.com/gitlab-org/release/tasks/issues
[Release Manager]: ../../quickstart/release-manager.md
